# Helm Chart for PostgreSQL
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) 

## Introduction

This [Helm](https://github.com/kubernetes/helm) chart  is a fork of [cetic/helm-postgresql](https://github.com/cetic/helm-postgresql)

It installs [postgreSQL](https://www.postgresql.org/) in a Kubernetes cluster.


## Prerequisites

- Kubernetes cluster 1.10+
- Helm 3.0.0+
- PV provisioner support in the underlying infrastructure.

## Installation

### Add Helm repository

```bash
helm repo add nettistanet https://nettistanet.codeberg.page/charts
helm repo update
```

### Configure the chart

The following items can be set via `--set` flag during installation or configured by editing the `values.yaml` directly (need to download the chart first).

#### Configure the way how to expose postgreSQL service:

- **ClusterIP**: Exposes the service on a cluster-internal IP. Choosing this value makes the service only reachable from within the cluster.
- **NodePort**: Exposes the service on each Node’s IP at a static port (the NodePort). You’ll be able to contact the NodePort service, from outside the cluster, by requesting `NodeIP:NodePort`.
- **LoadBalancer**: Exposes the service externally using a cloud provider’s load balancer.

#### Configure the way how to persistent data:

- **Disable**: The data does not survive the termination of a pod.
- **Persistent Volume Claim(default)**: A `StorageClass` is needed in the Kubernetes cluster to dynamic provision the volumes. Specify another `storageClass` or set `existingClaim` if you have already existing persistent volumes to use.

#### How to use LDAP (DEPRECATED):
LDAP is used only to validate the user name/password pairs. Therefore the user must already exist in the database before LDAP can be used for authentication. If you **enable ldap** a **CronJob** will be activated, it will run the tool [pg-ldap-sync](https://github.com/cetic/pg-ldap-sync-docker) that will copy users from your ldap server to your database.

#### How to use backup:
The backup-feature of this charts uses `pg_dump` to create a database-dump and then stores the dump in a Restic repository. It had been created with S3 storage in mind, but it should be possible to use it with other protocols supportd by Restic as well.

The specific environment-variables can be specified using `backup.extraEnv`.

Either way you need to provide a secret containing the keys `repository-location` and `repository-password`. The default name for the secret is `restic`. If you want to use another name you need to override the value of `restic.existingSecret`.

`pg_dump` uses `postgresql.existingSecret` to connect to the database.

The value of `backup.schedule` is used to define when `pg_dump` should run.

If you don't want to use Restic e.g. because you want to use mechanisms provided by the 

##### Restore
In order to restore a backup you can use the `restore.sh` provided in configmap `restic`. In order to use it you could create a Job similar to below and excute using `kubectl apply -f <path to file> --namespace <name of namespace with db>`

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: postgresql-restore
spec:
  template:
    spec:
      containers:
      - name: postgresql-restore
        image:  postgres:12
        command:
          - /bin/sh
          - -c
          - /restore.sh
        env:
        - name: PGPASS
          valueFrom:
            secretKeyRef:
              name: postgresql
              key: password
        - name: PGUSER
          valueFrom:
            secretKeyRef:
              name: postgresql
              key: username
        - name: PGHOST
          value: "postgresql" # Make sure this matches the hostname of you target PostgreSQL instance
        - name: PGDATABASE
          valueFrom:
            secretKeyRef:
              name: postgresql
              key: database
        - name: RESTIC_REPOSITORY
          valueFrom:
            secretKeyRef:
              name: restic
              key: repository-location
        - name: RESTIC_PASSWORD
          valueFrom:
            secretKeyRef:
              name: restic
              key: repository-password
        - name: AWS_ACCESS_KEY_ID # Make sure to adapt that to your specific setup
          valueFrom:
            secretKeyRef:
              name: restic
              key: s3-key-id
        - name: AWS_SECRET_ACCESS_KEY # Make sure to adapt that to your specific setup
          valueFrom:
            secretKeyRef:
              name: restic
              key: s3-key-secret
        - name: NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: RESTORE_ID # ID of backup or latest
          value: "latest"
        volumeMounts:
        - name: restore-backup-script
          mountPath: "/restore.sh"
          subPath: "restore.sh"
          readOnly: false
      restartPolicy: Never
      volumes:
      - name: restore-backup-script
        configMap:
          name: postgresql-backup
          items:
          - key: restore.sh
            path: restore.sh
          defaultMode: 0777
```

### Install the chart

Install the postgresql helm chart with a release name `my-release`:

```bash
helm install --name my-release nettistanet/postgresql
```

## Uninstallation

To uninstall/delete the `my-release` deployment:

```bash
helm delete --purge my-release
```

## Configuration

The following table lists the configurable parameters of the postgresql chart and the default values.

| Parameter                                                                   | Description                                                                                                        | Default                         |
| --------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------| ------------------------------- |
| **Image**                                                                   |
| `image.repository`                                                          | postgresql Image name                                                                                              | `postgres`                      |
| `image.tag`                                                                 | postgresql Image tag                                                                                               | `11.5`                          |
| `image.pullPolicy`                                                          | postgresql Image pull policy                                                                                       | `IfNotPresent`                  |
| `image.pullSecret`                                                          | postgresql Image pull secret                                                                                       | `nil`                           |
| **postgresql properties**                                                   |
| `postgresql.username`                                                       | postgresql username                                                                                                | `postgres`                      |
| `postgresql.password`                                                       | postgresql password                                                                                                | `postgres`                      |
| `postgresql.database`                                                       | postgresql database                                                                                                | `postgres`                      |
| `postgresql.existingSecret`                                                 | Name existing secret which has to contain the keys `username`, `password`, `database`                              | `nil`                           |
| `postgresql.port`                                                           | postgresql port                                                                                                    | `5432`                          |
| `postgresql.dataDir`                                                        | PostgreSQL data dir folder                                                                                         | `/var/lib/postgresql/data/pgdata`|
| `postgresql.config`                                                         | Runtime Config Parameters                                                                                          | `nil`                           |
| `postgresql.pghba`                                                          | Content of pg\_hba.conf                                                                                            | `nil (do not create pg_hba.conf)`|
| `postgresql.initdbscripts`                                                  | Content of initdbscripts.sh ( commands to be executed at the start of postgres )                                   | `nil (do not create initdbscripts.sh)`|
| `postgresql.configMap`                                                      | ConfigMap with the PostgreSQL configuration files (Note: Overrides `postgresqlConfiguration` and `pgHbaConfiguration`). The value is evaluated as a template. | `nil`|
| `extraEnv`                                                                  | Any extra environment variables you would like to pass on to the pod. The value is evaluated as a template.        | `{}`                            |
| **Service**                                                                 |
| `service.type`                                                              | Type of service for postgresql frontend                                                                            | `CusterIP`                      |
| `service.loadBalancerIP`                                                    | LoadBalancerIP if service type is `LoadBalancer`                                                                   | `nil`                           |
| `service.clusterIP`                                                         | ClusterIP if service type is `ClusterIP`                                                                           | `nil`                           |
| `service.loadBalancerSourceRanges`                                          | Address that are allowed when svc is `LoadBalancer`                                                                | `[]`                            |
| `service.annotations`                                                       | Service annotations                                                                                                | `{}`                            |
| **Volume Permissions**                                                      |
| `volumePermissions.image.registry`                                          | Init container volume-permissions image registry                                                                   | `docker.io`                     |
| `volumePermissions.image.repository`                                        | Init container volume-permissions image name                                                                       | `debian`                        |
| `volumePermissions.image.tag`                                               | Init container volume-permissions image tag                                                                        | `buster-slim`                   |
| `volumePermissions.image.pullPolicy`                                        | Init container volume-permissions image pull policy                                                                | `Always`                        |
| `volumePermissions.securityContext.runAsUser`                               | User ID for the init container                                                                                     | `0`                             |
| `volumePermissions.securityContext.privileged`                              | Run container in privileged mode.                                                                                  | `false`                         |
| **Security Context**                                                        |
| `securityContext.enabled`                                                   | Enable security context                                                                                            | `true`                          |
| `securityContext.fsGroup`                                                   | Group ID for the container                                                                                         | `1001`                          |
| `securityContext.runAsUser`                                                 | User ID for the container                                                                                          | `1001`                          |
| **LDAP**                                                                    |
| `ldap.enabled`                                                              | Use ldap authentication                                                                                            | `false`                         |
| `ldap.pgldapconfig`                                                         | pgldap config file                                                                                                 | ``                              |
| `ldap.cron.schedule`                                                        | Cron job schedule                                                                                                  | `""`                            |
| `ldap.cron.repo`                                                            | Cron job Docker image                                                                                              | `ceticasbl/pg-ldap-sync`        |
| `ldap.cron.tag`                                                             | Cron job Docker image tag                                                                                          | `latest`                        |
| `ldap.cron.restartPolicy`                                                   | Restart policy of the cron job                                                                                     | `Never`                         |
| `ldap.cron.mountPath`                                                       | Path to mount the volume at                                                                                        | ``                              |
| `ldap.cron.subPath`                                                         | Subdirectory of the volume to mount at                                                                             | ``                              |
| **Persistence**                                                             |
| `persistence.enabled`                                                       | Use persistent volume to store data                                                                                | `false`                         |
| `persistence.mountPath`                                                     | Path to mount the volume at                                                                                        | `/var/lib/postgresql`           |
| `persistence.subPath`                                                       | Subdirectory of the volume to mount at                                                                             | `""`                            |
| `persistence.storageClass`                                                  | Storage class name of PVCs                                                                                         | `standard`                      |
| `persistence.accessMode`                                                    | ReadWriteOnce or ReadOnly                                                                                          | `[ReadWriteOnce]`               |
| `persistence.size`                                                          | Size of persistent volume claim                                                                                    | `10Gi`                          |
| **Backup**                                                                  |
| `backup.enabled`                                                            | Create pg_dumps regularly                                                                                          | `false`                         |
| `backup.rententionInDays`                                                   | How many days a backup is kept.                                                                                    | `30`                            |
| `backup.restartPolicy`                                                      | Restart policy of the cron job.                                                                                    | `Never`                         |
| `backup.schedule`                                                           | Cron job schedule.                                                                                                 | `15 4 * * *`                    |
| `backup.existingSecret`                                                     | Name of existing secret which has to contain the keys `repository-location` and `repository-password`.             | `restic`                        |
| `backup.extraEnv`                                                           | Extra env-vars for backup cronjob. Please refer to the [Restic documentation](https://restic.readthedocs.io/en/latest/040_backup.html#environment-variables) for information about environment-variables for supported protocols.    | `{}` |
| **ReadinessProbe**                                                          |
| `readinessProbe`                                                            | Rediness Probe settings                                                                                            | `nil`                           |
| **LivenessProbe**                                                           |
| `livenessProbe`                                                             | Liveness Probe settings                                                                                            | `nil`                           |
| **Resources**                                                               |
| `resources`                                                                 | Pod resource requests and limits for logs                                                                          | `{}`                            |
| **nodeSelector**                                                            |
| `nodeSelector`                                                              | Node labels for pod assignment                                                                                     | `{}`                            |
| **tolerations**                                                             |
| `tolerations`                                                               | Tolerations for pod assignment                                                                                     | `[]`                            |

## Why this PostgreSQL Helm Chart?

* use postgres official Docker Image.
* use this Chart for other ARM64 architecture.
* ...

## Contributing

Feel free to contribute by making a [pull request](https://codeberg.org/saretter/helm-postgresql/pulls).

## License

[Apache License 2.0](/LICENSE)
