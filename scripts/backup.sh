#!/bin/sh

# Download restic
apt update
apt install restic -y

# Install ca-certificates
apt-get install ca-certificates -y

# Prepare for pg_dump
echo "$PGHOST:5432:$PGDATABASE:$PGUSER:$PGPASS" > /root/.pgpass
chmod 600 /root/.pgpass
mkdir -p /tmp/backup/$NAMESPACE

# Execute pg_dump
pg_dump --clean > /tmp/backup/$NAMESPACE/$PGHOST-$PGDATABASE.sql

# Clean old dumps
find /tmp/backup/ \( -name '*' \) -mtime $BACKUP_RETENTION_DAYS -exec rm -rf {} \;

# Check if restic repository is initialized and initialize if not
if ! restic --no-cache snapshots; then
    echo "Repo doesn't exist. Creating it ..."
    restic --no-cache init
fi

# Backup to restic-repository
restic --no-cache backup /tmp/backup/$NAMESPACE/

# Prune old backups
restic --no-cache forget --keep-last $BACKUP_RETENTION_DAYS