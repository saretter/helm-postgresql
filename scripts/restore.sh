#!/bin/sh

# Download restic
apt update
apt install restic -y

# Install ca-certificates
apt install ca-certificates -y

# Prepare for pg_dump
echo "$PGHOST:5432:$PGDATABASE:$PGUSER:$PGPASS" > /root/.pgpass
chmod 600 /root/.pgpass
mkdir -p /tmp/backup/$NAMESPACE

# Check if restic repository is initialized and initialize if not
if ! restic --no-cache snapshots; then
    exit -1
fi

# Restore backup to /tmp/backup
restic --no-cache restore $RESTORE_ID --target /

# Import pg_dump
psql < "/tmp/backup/$NAMESPACE/$PGHOST-$PGDATABASE.sql"